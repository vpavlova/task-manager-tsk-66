package ru.vpavlova.tm.service.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.vpavlova.tm.api.service.dto.IService;
import ru.vpavlova.tm.dto.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @Autowired
    protected ApplicationContext context;

}
