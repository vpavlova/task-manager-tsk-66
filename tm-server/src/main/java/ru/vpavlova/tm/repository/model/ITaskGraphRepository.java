package ru.vpavlova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.vpavlova.tm.api.repository.IGraphRepository;
import ru.vpavlova.tm.entity.TaskGraph;

import java.util.List;
import java.util.Optional;

public interface ITaskGraphRepository extends IGraphRepository<TaskGraph> {

    @Modifying
    @Query("DELETE FROM Task e")
    void clear();

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<TaskGraph> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<TaskGraph> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<TaskGraph> findOneByUserIdAndId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Optional<TaskGraph> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    @NotNull
    Optional<TaskGraph> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :taskId")
    void bindTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("taskId") @NotNull String taskId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskFromProjectId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.user.id = :userId and e.name = :name")
    void removeByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

}
