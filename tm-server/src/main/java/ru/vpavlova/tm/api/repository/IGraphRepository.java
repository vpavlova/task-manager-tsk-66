package ru.vpavlova.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vpavlova.tm.entity.AbstractGraphEntity;

public interface IGraphRepository<E extends AbstractGraphEntity> extends JpaRepository<E, String> {

}

