package ru.vpavlova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.vpavlova.tm")
public class ApplicationConfiguration {

}