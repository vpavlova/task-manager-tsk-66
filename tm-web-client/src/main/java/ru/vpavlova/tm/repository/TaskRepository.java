package ru.vpavlova.tm.repository;

import org.springframework.stereotype.Repository;
import ru.vpavlova.tm.api.repository.ITaskRepository;
import ru.vpavlova.tm.model.Task;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class TaskRepository implements ITaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        create();
        create();
        create();
    }

    @Override
    public void create() {
        Task task = new Task("task", "new");
        tasks.put(task.getId(), task);
    }

    @Override
    public void removeById(final String id) {
        tasks.remove(id);
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values()) ;
    }

    @Override
    public Task findById(final String id) {
        return tasks.get(id);
    }

    @Override
    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void saveAll(final List<Task> list) {
        tasks.putAll(list.stream().collect(Collectors.toMap(Task::getId, Function.identity())));
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

}
